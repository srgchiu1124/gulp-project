const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var pipeline = require('readable-stream').pipeline;
var sourcemaps = require('gulp-sourcemaps');
var rename        = require('gulp-rename');

gulp.task('minifyCss', function () {
    return gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      errLogToConsole: true,
      outputStyle: 'expanded' // compiles SASS to CSS
    }))

    .pipe(rename('style.css')) // not yet minified at this point, just compiled [optional]
    .pipe(sass({
      errLogToConsole: true,
      outputStyle: 'compressed' // minifies style.min.css
    }))

    .pipe(sourcemaps.write('/', { // write style.min.css.map to same directory as style.min.css
        includeContent: false,
        sourceRoot: '../../src/sass' // relative to minified output location
      }))

      .pipe(gulp.dest('dist/css/')) // tells task which directory to output minified CSS
  });

gulp.task('minifyJs', function () {
    return pipeline(
          gulp.src('src/js/*.js'),
          uglify(),
          gulp.dest('dist/js')
    );
  });

  gulp.task('minifyHtml', () => {
    return gulp.src('src/*.html')
      .pipe(htmlmin({ collapseWhitespace: true }))
      .pipe(gulp.dest('dist'));
  });

  gulp.task('copy-img', function() {
    return gulp.src('src/images/*')
      .pipe(gulp.dest('dist/images/'));
  });

  async function style() {
    return gulp.src('src/scss/*.scss')
    .pipe(sass().on('error',sass.logError))
    .pipe(gulp.dest('src/css/'))
    .pipe(browserSync.stream());
}

  async function watch() {
    browserSync.init({
        server: {
           baseDir: "./src",
           index: "/index.html"
        }
    });
    gulp.watch('src/scss/**/*.scss', style)
    gulp.watch('src/*.html').on('change',browserSync.reload);
    gulp.watch('src/js/*.js').on('change', browserSync.reload);
}

gulp.task('dist', gulp.series( 'minifyCss','minifyJs' , 'minifyHtml' , 'copy-img'));


exports.style = style;
exports.watch = watch;
