document.addEventListener("DOMContentLoaded", function() {
    console.log("ready");

    var card_link = document.querySelectorAll("a.card_link-a");
    console.log(card_link.length);

    var linkCount = function() {
      var els = Array.prototype.slice.call( card_link, 0 );
      console.log("Card Link " + els.indexOf(event.currentTarget));
    };

    for (var i = 0; i < card_link.length; i++) {
      card_link[i].addEventListener('click', linkCount, false);
    }

    //****************** MODAL **************/
    var img_tile = document.querySelectorAll("img.tile_img");
    var modal_close = document.querySelector(".CloseModal");

    function addElement() {
      // create a new div element
      const newDiv = document.createElement("div");

      newDiv.classList.add("CloseModal");
    
      // and give it some content
      const newContent = document.createTextNode("");
    
      // add the text node to the newly created div
      newDiv.appendChild(newContent);
    
      // add the newly created element and its content into the DOM
      const currentDiv = document.getElementById("div1");
      document.body.insertBefore(newDiv, currentDiv);
    }

    function modal() {
      for  ( var i = 0; i < img_tile.length; i++){
        img_tile[i].addEventListener('click', function (){
          this.parentElement.classList.toggle("modal-open");
          document.body.classList.toggle('modal');
          addElement();
        });
      };
  }
  modal();

});